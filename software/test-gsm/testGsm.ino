// Connect VCC to +5V
// Connect GND to Ground
// Connect RXD (data into SIM800L) to Digital 8
// Connect TXD (data out from SIM800L) to Digital 7
// #include <SoftwareSerial.h>
// SoftwareSerial sim800l(7, 8);  // RX, TX
// String SIM_PIN_CODE = String("0000");

// void setup() {
//     Serial.begin(9600);
//     sim800l.begin(9600);

//     //  sim800l.print("AT+CPIN=");
//     // sim800l.println( SIM_PIN_CODE );
//     // updateSerial();
//     sim800l.println("AT+CMGF=1");  // Configuring TEXT mode
//     updateSerial();
//     sim800l.println(
//         "AT+CMGS=\"+33665099317\"");  // change ZZ with country code and
//                                       // xxxxxxxxxxx with phone number to sms
//     updateSerial();
//     sim800l.print("Bisous");  // text content
//     updateSerial();
//     sim800l.write(26);
// }

// void loop() {
//     if (sim800l.available())
//         Serial.write(sim800l.read());

//     if (Serial.available()) {
//         while (Serial.available()) {
//             sim800l.write(Serial.read());
//         }
//         sim800l.println();
//     }
// }

// void updateSerial() {
//     delay(500);
//     while (Serial.available()) {
//         sim800l.write(Serial.read());  // Forward what Serial received to
//                                        // Software Serial Port
//     }
//     while (sim800l.available()) {
//         Serial.write(sim800l.read());  // Forward what Software Serial
//         received
//                                        // to Serial Port
//     }
// }

// #include <SIM800.h>
#include "BareBoneSim800.h"


BareBoneSim800 sim800;
const int MAX_ATTEMPTS = 10;

void setup() {
    sim800.begin();
    Serial.begin(9600);
    while (!Serial) {
    }
    Serial.println("cooot");

    Serial.println("Start");
    _wakeUpGsmMaybe();

    for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        // if (sim800.sendSMS("+33647604130", "message 1")) {
        if (sim800.sendSMS("+33658438598", "message 1")) {
            Serial.print("Message 1 sent. Attempts: ");
            Serial.println(attempt);
            break;
        }
        delay(1000);
    }
    Serial.println("Message 1 sent.");

    // waitGsmInSleepMode();

    // delay(5000);

    // _wakeUpGsmMaybe();

    // for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
    //     if (sim800.sendSMS("+33665099317", "message 2")) {
    //         Serial.print("Message 2 sent. Attempts: ");
    //         Serial.println(attempt);
    //         break;
    //     }
    //     delay(1000);
    // }
    // Serial.println("Message 2 sent.");
}

void loop() {
    static int currentMessageIndex = 0;
    String message = "";
    if (sim800.checkNewSMS() ||
        currentMessageIndex < sim800.currentMessageIndex) {
        Serial.println("new sms !");
        Serial.println(sim800.currentMessageIndex);
        // message = sim800.readSMS(sim800.currentMessageIndex);
        currentMessageIndex++;
        message = sim800.readSMS(currentMessageIndex);
    } else if (currentMessageIndex > 0) {
        sim800.dellAllSMS();
        currentMessageIndex = 0;
    }
    if (Serial.available()) {
        message = Serial.readStringUntil('\r');
    }
    if (message != "") {
        message.toLowerCase();
        Serial.println(message);
        if (message.indexOf("appel") >= 0) {
            const int NUMBER_DELIMITER_SIZE = 12;
            const int NUMBER_SIZE = 12;
            const char numberDelimiter[NUMBER_DELIMITER_SIZE + 1] =
                "\"rec read\",\"";
            Serial.println(message.indexOf(numberDelimiter));
            Serial.println(sizeof(numberDelimiter));
            Serial.println(sizeof(char));
            int numberStartIndex =
                message.indexOf(numberDelimiter) + NUMBER_DELIMITER_SIZE;
            String number = message.substring(numberStartIndex,
                                              numberStartIndex + NUMBER_SIZE);
            Serial.println(number);
            sim800.callNumber(number.c_str());
        }
    }
    delay(1000);
}

void _wakeUpGsmMaybe(void) {
    // for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
    //     if (sim800.disableSleep()) {
    //         Serial.print("Gsm awake. Attempts: ");
    //         Serial.println(attempt);
    //         break;
    //     }
    //     delay(1000);
    // }
    // Serial.println("Gsm awake.");

    // for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
    //     if (sim800.setFullMode()) {
    //         Serial.print("Gsm ready. Attempts: ");
    //         Serial.println(attempt);
    //         break;
    //     }
    //     delay(1000);
    // }
    // Serial.println("Gsm ready.");

    for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        if (sim800.isAttached()) {
            Serial.print("Gsm ready. Attempts: ");
            Serial.println(attempt);
            break;
        }
        delay(1000);
    }
    Serial.println("Gsm attached.");
}

void waitGsmInSleepMode(void) {
    for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        if (sim800.enterSleepMode()) {
            Serial.print("Gsm sleeping. Attempts: ");
            Serial.println(attempt);
            break;
        }
        delay(1000);
    }
    Serial.println("Gsm sleeping.");
}
