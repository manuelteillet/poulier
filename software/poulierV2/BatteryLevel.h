#ifndef BatteryLevel_h
#define BatteryLevel_h

#include "Arduino.h"

class BatteryLevel {
public:
    BatteryLevel(const int batteryLevelPin);
    void begin(void) const;
    unsigned int getMilliVoltValue(void) const;
    bool getError(void) const;
    void setError(bool error);

private:
    const int _batteryLevelPin;
    bool _error;
};

#endif
