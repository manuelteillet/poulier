#include "LightSensor.h"

LightSensor::LightSensor(const int lightSensorPin,
                         const int lightSensorEnablePin)
    : _lightSensorPin(lightSensorPin),
      _lightSensorEnablePin(lightSensorEnablePin) {}

void LightSensor::begin() const {
    pinMode(_lightSensorPin, INPUT);
    pinMode(_lightSensorEnablePin, OUTPUT);
}

int LightSensor::getValue() const {
    digitalWrite(_lightSensorEnablePin, HIGH);
    delay(500);
    int lightLevel = analogRead(_lightSensorPin);
    digitalWrite(_lightSensorEnablePin, LOW);
    return lightLevel;
}
