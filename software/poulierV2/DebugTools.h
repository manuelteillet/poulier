#ifndef DebugTools_h
#define DebugTools_h

#include "config/Poulier.h"

#ifdef _STATES_DEBUG
#define DEBUG_PRINT_STATE_NAME                      \
    {                                               \
        if (currentState != previousCurrentState) { \
            Serial.println(__FUNCTION__);           \
            delay(500);                             \
            previousCurrentState = currentState;    \
        }                                           \
    }
#else
#define DEBUG_PRINT_STATE_NAME
#endif

#ifdef _FREE_MEMORY_DEBUG
// #ifdef __arm__
// // should use uinstd.h to define sbrk but Due causes a conflict
// extern "C" char* sbrk(int incr);
// #else  // __ARM__
// extern char *__brkval;
// #endif  // __arm__

// int getFreeRam() {
//   char top;
// #ifdef __arm__
//   return &top - reinterpret_cast<char*>(sbrk(0));
// #elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
//   return &top - __brkval;
// #else  // __arm__
//   return __brkval ? &top - __brkval : &top - __malloc_heap_start;
// #endif  // __arm__
// }
// #define DEBUG_PRINT_FREE_MEMORY \
//     {                                               \
//     int freeMemory = getFreeRam(); \
//         if (freeMemory != previousFreeMemory) { \
//             Serial.print(F("Free memory : "));           \
//             Serial.println(freeMemory);           \
//             previousFreeMemory = freeMemory;    \
//         }                                           \
//     }    
#define DEBUG_PRINT_FREE_MEMORY \
    {  \
        char *__brkval; char top;                                           \
        int freeMemory = __brkval ? &top - __brkval : &top - __malloc_heap_start;; \
        Serial.print(F("Free memory : "));           \
        Serial.println(freeMemory);           \
        delay(500);                             \
    }
#else
#define DEBUG_PRINT_FREE_MEMORY
#endif

#ifdef _GSM_DEBUG
// #define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINTF(format, ...)                        \
    {                                                    \
        char buf[50];                                   \
        sprintf(buf, (const char *)format, __VA_ARGS__); \
        Serial.println(buf);                             \
        delay(500);                             \
    }
// #define DEBUG_PRINT(message)
#define DEBUG_PRINT(message) \
    {                                                    \
        Serial.println(message); \
        delay(500);                             \
    }
#else
#define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINT(message)
#endif

#if defined(_STATES_DEBUG) || defined(_FREE_MEMORY_DEBUG) || defined(_GSM_DEBUG) 
#define _SERIAL_NEEDED_FOR_DEBUG
#endif

#endif
