#include "Gsm.h"
#include "DebugTools.h"
#ifndef _GSM_DISABLE
#include "BareBoneSim800.h"
#endif

typedef struct {
    const commands command;
    const char keyword[10];
} commandKeyword;

const int MAX_ATTEMPTS = 5;
#ifndef _GSM_DISABLE
BareBoneSim800 sim800;
#endif
Gsm::Gsm(const char (*numbersToSendTo)[13], const int numbersToSendToSize)
    : _numbersToSendTo(numbersToSendTo),
      _numbersToSendToSize(numbersToSendToSize),
      _isMessageQueueEmpty(false),
      _isGsmInSleepMode(true) {
    strcpy(_numberToCall, _numbersToSendTo[0]);
}

void Gsm::begin() {
#ifndef _GSM_DISABLE
    delay(2000);
    sim800.begin();
    delay(2000);
    Gsm::_wakeUpGsmMaybe();
    sim800.dellAllSMS();
    sim800.flushSerial(1000);
    delay(1000);
#endif
}

commands Gsm::getTextCommand(void) {
    DEBUG_PRINT(F("Start getting text."))
    // const commandKeyword commandsKeywords[SIZE_OF_COMMANDS] = {
    //     {GET_SYSTEM_VALUES,
    //      {"valeurs", "systeme", "système", "lire", "lecture", "etat",
    //      "état"}},
    //     {FORCE_OPEN, {"ouvrir", "ouverture", "ouvert"}},
    //     {FORCE_CLOSE, {"fermer", "fermeture", "ferme", "fermé"}},
    //     {CALL, {"appeler", "appel"}}};
    const commandKeyword commandsKeywords[SIZE_OF_COMMANDS] = {
        {GET_SYSTEM_VALUES, "valeurs"},
        {FORCE_OPEN, "ouvrir"},
        {FORCE_CLOSE, "fermer"},
        {CALL, "appel"}};
    Gsm::_wakeUpGsmMaybe();

    String message = "";
#ifndef _GSM_DISABLE
    static int currentMessageIndex = 0;
    if (sim800.checkNewSMS() ||
        currentMessageIndex < sim800.currentMessageIndex) {
        DEBUG_PRINTF("New sms. Index: %d, Sim800 index: %d\n",
                     currentMessageIndex, sim800.currentMessageIndex)
        currentMessageIndex++;
        delay(1000);

        message = sim800.readSMS(currentMessageIndex);
        sim800.flushSerial(1000);
        delay(1000);

    } else {
        DEBUG_PRINT(F("No new text."))
        _isMessageQueueEmpty = true;
        if (currentMessageIndex > 0) {
            sim800.dellAllSMS();
            delay(1000);

            currentMessageIndex = 0;
        }
    }
#else
    _isMessageQueueEmpty = true;
#endif
#ifdef _GSM_DEBUG
    if (Serial.available()) {
        message = Serial.readStringUntil('\r');
    }
    while (Serial.available() > 0) {
    }
#endif
    if (message != "") {
        DEBUG_PRINT(message)
        // return NO_COMMAND;

        message.toLowerCase();
        for (int commandIndex = 0; commandIndex < SIZE_OF_COMMANDS;
             commandIndex++) {
            if (message.indexOf(commandsKeywords[commandIndex].keyword) >= 0) {
                if (commandsKeywords[commandIndex].command == CALL) {
                    const int NUMBER_DELIMITER_SIZE = 12;
                    const int NUMBER_SIZE = 12;
                    const char numberDelimiter[NUMBER_DELIMITER_SIZE + 1] =
                        "\"rec read\",\"";
                    int numberStartIndex = message.indexOf(numberDelimiter) +
                                           NUMBER_DELIMITER_SIZE;
                    strcpy(_numberToCall,
                           message
                               .substring(numberStartIndex,
                                          numberStartIndex + NUMBER_SIZE)
                               .c_str());
                }
                return commandsKeywords[commandIndex].command;
            }
        }
    }
    return NO_COMMAND;
}

void Gsm::sendText(const __FlashStringHelper *pMessage) {
    char message[200];
    strcpy_P(message, (char *)pMessage);
    Gsm::sendText(message);
}

void Gsm::sendText(const char *message) {
    DEBUG_PRINT(F("Start sending text."))
    Gsm::_wakeUpGsmMaybe();
    for (int numberIndex = 0; numberIndex < _numbersToSendToSize;
         numberIndex++) {
#ifndef _GSM_DISABLE
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.sendSMS(_numbersToSendTo[numberIndex], message)) {
                DEBUG_PRINTF("Text sent. Attempt: %d", attempt)
                break;
            }
            delay(1000);
        }
        sim800.flushSerial(1000);
        delay(1000);

#endif
        DEBUG_PRINTF("Text to %s: %s\0", _numbersToSendTo[numberIndex], message)
    }
}

void Gsm::call(void) const {
#ifndef _GSM_DISABLE
    sim800.callNumber(_numberToCall);
    delay(1000);
#endif
    DEBUG_PRINTF("Calling %s\0", _numberToCall);
}

messageQueueStates Gsm::getMessageQueueState(void) const {
    return _isMessageQueueEmpty ? EMPTY : NOT_EMPTY;
}

void Gsm::setGsmInSleepMode(void) {
#ifndef _GSM_DISABLE
    if (!_isGsmInSleepMode) {
        delay(1000);
        DEBUG_PRINT(F("Trying to go to sleep."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            // DEBUG_PRINT_FREE_MEMORY
            if (sim800.enterSleepMode()) {
                DEBUG_PRINTF("Gsm sleeping. Attempt: %d", attempt)
                _isGsmInSleepMode = true;
                break;
            }
            delay(1000);
        }
        sim800.flushSerial(1000);
        delay(1000);
    }
#endif
}

void Gsm::_wakeUpGsmMaybe(void) {
#ifndef _GSM_DISABLE
    if (_isGsmInSleepMode) {
        DEBUG_PRINT(F("Trying to wake up."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
                            DEBUG_PRINT_FREE_MEMORY

            if (sim800.disableSleep()) {
                // DEBUG_PRINT_FREE_MEMORY
                DEBUG_PRINTF("Gsm awake. Attempt: %d", attempt)
                // DEBUG_PRINT_FREE_MEMORY
                break;
            }
            delay(1000);
        }
        delay(1000);
        sim800.flushSerial(1000);
        DEBUG_PRINT(F("Trying to set full mode."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
                            DEBUG_PRINT_FREE_MEMORY

            if (sim800.setFullMode()) {
                DEBUG_PRINTF("Gsm full mode. Attempt: %d", attempt)
                break;
            }
            delay(1000);
        }
        delay(1000);
        sim800.flushSerial(1000);
        DEBUG_PRINT(F("Trying to attach."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
                            DEBUG_PRINT_FREE_MEMORY

            if (sim800.isAttached()) {
                DEBUG_PRINTF("Gsm attached. Attempt: %d", attempt)
                _isGsmInSleepMode = false;
                break;
            }
            delay(1000);
        }
        sim800.flushSerial(1000);
        delay(1000);
    }
#endif
}
