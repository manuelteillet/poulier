#ifndef ConfigPoulier_h
#define ConfigPoulier_h

// #define _SLEEP_DISABLE
// #define _GSM_DISABLE

#define _STATES_DEBUG
#define _FREE_MEMORY_DEBUG
#define _GSM_DEBUG

#define MANU_AND_AUDREY_PHONE_NUMBERS {"+33658438598", "+33665099317"}
#define MANU_ONLY_PHONE_NUMBER {"+33658438598"}
#define PHONE_NUMBERS_LIST MANU_ONLY_PHONE_NUMBER
#define QUARTER_HOUR_WAKE_UP_LIMIT 90
#define DEBUG_WAKE_UP_LIMIT 2
#define WAKE_UP_LIMIT DEBUG_WAKE_UP_LIMIT

// pin descriptions
const int LIFE_SIGNAL_PIN = 13;
const int LIGHT_SENSOR_PIN = A0;
const int LIGHT_SENSOR_ENABLE_PIN = 7;
const int BATTERY_LEVEL_PIN = A1;
const int MOTOR_UP_PIN = 5;
const int MOTOR_DOWN_PIN = 6;
const int DOOR_SENSOR_PIN = 3;
const int BUTTON_PIN = 2;
// GSM_RX_PIN = 8;
// GSM_TX_PIN = 9;

#endif