# Poulier micropython

## Scripts

### Read log

To extract log files to a local file, use

`rshell -p /dev/ttyUSB0 'repl ~ import scripts ~ scripts.read_logs(200) ~ print("finished") ' | tee test.log`

and press `Ctrl`+`X` when `finished` is displayed.

### Set clock
