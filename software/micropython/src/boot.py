# boot.py - - runs on boot-up
# import webrepl
# import network


# webrepl.start()

# ap = network.WLAN(network.AP_IF)
# ap.active(True)
# ap.config(essid="esp32", password="")

import machine


MAX_SPEED = 240000000
NORMAL_SPEED = 160000000
MIN_SPEED = 80000000

speed = MAX_SPEED


machine.freq(speed)
