import time
from machine import Pin, TouchPad
import webrepl
import network
import esp32

from lib.logging import getLogger, basicConfig, DEBUG
from lib.config import get_config
from lib.domain.maintenance_mode import (
    is_maintenance_mode_button_pressed,
    maintenance_mode,
)
from lib.state_machine import StateMachine, Transition, next, idle
from lib.sleep import deep_sleep, get_reset_cause_and_wake_reason

transition_table = [
    Transition(
        "idle", is_maintenance_mode_button_pressed, "maintenance_mode", maintenance_mode
    ),
    Transition("maintenance_mode", next, "deep_sleep", deep_sleep),
    Transition("idle", next, "deep_sleep", deep_sleep),
]

main_state_machine = StateMachine("idle", transition_table)


def main():
    basicConfig(level=DEBUG)
    logger = getLogger("main")
    logger.info(
        "Booting up. %s",
        get_reset_cause_and_wake_reason(),
    )

    main_state_machine.run()


main()
