from time import sleep, localtime, mktime
from network import WLAN, STA_IF
import ntptime
from lib.logging import read_logs as _read_logs
from lib.config import get_config


def set_rtc_from_ntp():
    config = get_config()

    wlan = WLAN(STA_IF)
    wlan.active(True)
    while not wlan.isconnected():
        print("trying to connect to ssid {}".format(config.wan_access_ssid))
        wlan.connect(config.wan_access_ssid, config.wan_access_passphrase)
        sleep(8)

    now = ntptime.time()
    (year, month, mdate, hour, minute, second, wday, yday) = localtime(now)
    dstend = mktime((year, 10, (31 - (int(5 * year / 4 + 1)) % 7), 4, 0, 0, 0, 6, 0))
    dstbegin = mktime((year, 3, (31 - (int(5 * year / 4 + 4)) % 7), 3, 0, 0, 0, 6, 0))

    if config.timezone_offset >= 0:
        if (now > dstbegin) and (now < dstend):
            dst_on = True
            ntptime.NTP_DELTA = 3155673600 - ((config.timezone_offset + 1) * 3600)
        else:
            dst_on = False
            ntptime.NTP_DELTA = 3155673600 - (config.timezone_offset * 3600)
    else:
        if (now > dstend) and (now < dstbegin):
            dst_on = False
            ntptime.NTP_DELTA = 3155673600 - (config.timezone_offset * 3600)
        else:
            dst_on = True
            ntptime.NTP_DELTA = 3155673600 - ((config.timezone_offset + 1) * 3600)

    ntptime.settime()
    wlan.disconnect()
    print(localtime())


def read_logs(nb_of_logs_to_read=100):
    _read_logs(nb_of_logs_to_read)
