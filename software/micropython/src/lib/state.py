import json


class State:
    def __init__(self):
        with open("state.json") as f:
            state = json.load(f)
        for key in state:
            super(State, self).__setattr__(key, state[key])

    def __setattr__(self, name, value):
        # print("Setting attribute {} to value {}".format(name, value))
        super(State, self).__setattr__(name, value)
        with open("state.json", "w") as f:
            json.dump(self.__dict__, f)
