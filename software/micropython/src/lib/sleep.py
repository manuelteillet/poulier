import esp32
import time
from machine import (
    reset_cause,
    wake_reason,
    PWRON_RESET,
    HARD_RESET,
    WDT_RESET,
    DEEPSLEEP_RESET,
    SOFT_RESET,
    PIN_WAKE,
    ULP_WAKE,
    TIMER_WAKE,
    TOUCHPAD_WAKE,
    EXT0_WAKE,
    EXT1_WAKE,
)

from lib.domain.maintenance_mode import get_maintenance_mode_pin
from lib.logging import getLogger
from machine import deepsleep
from lib.config import get_config


_reset_causes = {
    PWRON_RESET: "power on reset",
    HARD_RESET: "hard reset",
    WDT_RESET: "watchdog reset",
    DEEPSLEEP_RESET: "deep sleep reset",
    SOFT_RESET: "soft reset",
}

_wake_reasons = {
    PIN_WAKE: "pin wake",
    ULP_WAKE: "ulp wake",
    TIMER_WAKE: "timer wake",
    TOUCHPAD_WAKE: "touchpad wake",
    EXT0_WAKE: "ext0 wake",
    EXT1_WAKE: "ext1 wake",
}


def _init_wake_pins():
    esp32.wake_on_ext1(pins=[get_maintenance_mode_pin()], level=esp32.WAKEUP_ANY_HIGH)


_init_wake_pins()


def deep_sleep():
    logger = getLogger("sleep")
    config = get_config()
    logger.info("Going to deep sleep for %d min.", config.deep_sleep_time_minute)
    deepsleep(int(config.deep_sleep_time))


def get_reset_cause_and_wake_reason():
    return "Reset cause is: %s" % (
        "%s and wake reason is: %s"
        % (_reset_causes[reset_cause()], _wake_reasons[wake_reason()])
        if wake_reason()
        else _reset_causes[reset_cause()]
    )
