from time import sleep_ms


class PoulierError(Exception):
    pass


def debounce(input_pin, timeout_ms=200, stable_time_ms=50):
    time_cnt = 0
    while time_cnt < timeout_ms:
        first_value = input_pin.value()
        for stable in range(stable_time_ms + 1):
            sleep_ms(1)
            time_cnt += 1
            second_value = input_pin.value()
            if first_value != second_value:
                break
        if stable == stable_time_ms:
            return first_value

    raise PoulierError("Could not get stable value for %s" % input_pin)
