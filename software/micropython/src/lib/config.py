import json
from collections import namedtuple


def get_config():
    with open("config.json") as f:
        config = json.load(f)

    config["deep_sleep_time"] = config["deep_sleep_time_minute"] * 60000

    return namedtuple("config", list(config.keys()))(*config.values())
