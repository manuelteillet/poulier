from lib.logging import getLogger
from time import sleep_ms


class StateMachine:
    def __init__(self, initial_state, transition_table):
        self.state = initial_state
        self.transition_table = transition_table

    def run(self):
        while True:
            self.run_once()
            sleep_ms(100)

    def run_once(self):
        for transition in self.transition_table:
            if transition.from_state == self.state and transition.trigger() == True:
                logger = getLogger("state_machine")

                logger.debug(
                    "Trigger %s is true. Going from state %s to state %s ",
                    transition.trigger.__name__,
                    transition.from_state,
                    transition.to_state,
                )

                if transition.callback:
                    logger.debug(
                        "Executing %s",
                        transition.callback.__name__,
                    )
                    transition.callback()

                self.state = transition.to_state

                break


class Transition:
    def __init__(self, from_state, trigger, to_state, callback):
        self.from_state = from_state
        self.trigger = trigger
        self.to_state = to_state
        self.callback = callback


def next():
    return True
