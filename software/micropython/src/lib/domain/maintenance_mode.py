from machine import Pin
from time import sleep
import webrepl
import network
from lib.logging import getLogger
from lib.tools import debounce
from lib.config import get_config

_config = get_config()
_maintenance_mode_pin = Pin(
    _config.maintenance_button_pin, mode=Pin.IN, pull=Pin.PULL_DOWN
)


def get_maintenance_mode_pin():
    return _maintenance_mode_pin


def get_maintenance_mode_pin_value():
    return debounce(_maintenance_mode_pin)


def is_maintenance_mode_button_pressed():
    return get_maintenance_mode_pin_value() == 1


def maintenance_mode(timeout_s=60):
    logger = getLogger("maintenance")
    logger.info("Entering maintenance mode for %ds", timeout_s)
    ap = network.WLAN(network.AP_IF)
    ap.config(essid=_config.access_point_ssid, password=_config.access_point_passphrase)
    ap.active(True)
    webrepl.start()

    sleep(timeout_s)

    webrepl.stop()
    ap.active(False)
    logger.info("Timeout elapsed.Exiting maintenance mode")
