import sys
from time import localtime
from state import State
import os, errno

CRITICAL = 50
ERROR = 40
WARNING = 30
INFO = 20
DEBUG = 10
NOTSET = 0

_level_dict = {
    CRITICAL: "CRIT",
    ERROR: "ERROR",
    WARNING: "WARN",
    INFO: "INFO",
    DEBUG: "DEBUG",
}

_stream = sys.stderr


class Logger:

    level = NOTSET

    def __init__(self, name):
        self.name = name

    def _level_str(self, level):
        l = _level_dict.get(level)
        if l is not None:
            return l
        return "LVL%s" % level

    def setLevel(self, level):
        self.level = level

    def isEnabledFor(self, level):
        return level >= (self.level or _level)

    def log(self, level, msg, *args):
        self._log(level, msg, *args)
        _manage_log_file_rotation()

    def _log(self, level, msg, *args):
        if level >= (self.level or _level):
            log_file_name = _build_log_file_name(State().log_file_index)
            with open(log_file_name, "a") as f:
                for stream in (f, _stream):
                    (year, month, day, hour, minute, second, wday, yday) = localtime()
                    stream.write(
                        "%d-%02d-%02d %02d:%02d:%02d|%s|%s|"
                        % (
                            year,
                            month,
                            day,
                            hour,
                            minute,
                            second,
                            self._level_str(level),
                            self.name,
                        )
                    )
                    if not args:
                        print(msg, file=stream)
                    else:
                        print(msg % args, file=stream)

    def debug(self, msg, *args):
        self.log(DEBUG, msg, *args)

    def info(self, msg, *args):
        self.log(INFO, msg, *args)

    def warning(self, msg, *args):
        self.log(WARNING, msg, *args)

    def error(self, msg, *args):
        self.log(ERROR, msg, *args)

    def critical(self, msg, *args):
        self.log(CRITICAL, msg, *args)

    def exc(self, e, msg, *args):
        self.log(ERROR, msg, *args)
        sys.print_exception(e, _stream)

    def exception(self, msg, *args):
        self.exc(sys.exc_info()[1], msg, *args)


_level = INFO
_loggers = {}


def getLogger(name):
    if name in _loggers:
        return _loggers[name]
    l = Logger(name)
    _loggers[name] = l
    return l


def info(msg, *args):
    getLogger(None).info(msg, *args)


def debug(msg, *args):
    getLogger(None).debug(msg, *args)


def basicConfig(level=INFO, filename=None, stream=None, format=None):
    global _level, _stream
    _level = level
    if stream:
        _stream = stream
    if filename is not None:
        print("logging.basicConfig: filename arg is not supported")
    if format is not None:
        print("logging.basicConfig: format arg is not supported")


def _build_log_file_name(log_file_index):
    return "log/log%d" % (log_file_index)


def read_logs(nb_of_logs_to_read=100):
    first_log_to_read = -nb_of_logs_to_read
    current_log_file_index = State().log_file_index
    current_file_name = ""
    nb_logs_in_current_file = 0
    while True:
        current_file_name = _build_log_file_name(current_log_file_index)
        # print("Current file name: {}".format(current_file_name))
        nb_logs_in_current_file = sum(1 for line in open(current_file_name))
        # print("Number of logs in current file: {}".format(nb_logs_in_current_file))
        first_log_to_read = nb_logs_in_current_file + first_log_to_read
        if current_log_file_index == 0 and State().log_is_first_turn:
            first_log_to_read = 0
        if first_log_to_read >= 0:
            break
        current_log_file_index = (current_log_file_index - 1) % (
            State().log_max_file_index + 1
        )

    while True:
        with open(current_file_name) as f:
            for index in range(first_log_to_read):
                f.readline()
            while True:
                line = f.readline()
                if not line:
                    break
                print(line, end="")
        nb_of_logs_to_read -= nb_logs_in_current_file - first_log_to_read
        # print("Number of logs still to read: {}".format(nb_of_logs_to_read))
        if nb_of_logs_to_read <= 0:
            break
        current_log_file_index = (current_log_file_index + 1) % (
            State().log_max_file_index + 1
        )
        current_file_name = _build_log_file_name(current_log_file_index)
        # print("Current file name: {}".format(current_file_name))
        nb_logs_in_current_file = sum(1 for line in open(current_file_name))
        # print("Number of logs in current file: {}".format(nb_logs_in_current_file))
        first_log_to_read = 0


def _manage_log_file_rotation():
    logger = getLogger("logging")
    free_space = os.statvfs("/")[0] * os.statvfs("/")[3]
    if free_space < 500000 and State().log_is_first_turn:
        logger._log(
            INFO,
            "Disk is almost full. Setting log_is_first_turn to False and log_max_file_index to %s"
            % State().log_file_index,
        )
        State().log_max_file_index = State().log_file_index
        State().log_is_first_turn = False

    log_file_name = _build_log_file_name(State().log_file_index)
    log_file_size = os.stat(log_file_name)[6]
    if log_file_size > 100000:
        if State().log_is_first_turn:
            next_log_file_index = State().log_file_index + 1
        else:
            next_log_file_index = (State().log_file_index + 1) % (
                State().log_max_file_index + 1
            )
        logger._log(
            INFO,
            "Log file is full. Increasing log_file_index to %d. Remaining free space is %d"
            % (next_log_file_index, free_space),
        )
        next_log_file_name = _build_log_file_name(next_log_file_index)
        try:
            os.remove(next_log_file_name)
            logger._log(INFO, "Deleting %s" % next_log_file_name)
        except OSError as e:
            if e.args[0] != errno.ENOENT:
                raise

        State().log_file_index = next_log_file_index
        logger._log(INFO, "First log for log_file_index %d" % next_log_file_index)
