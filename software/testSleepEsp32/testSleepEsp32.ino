#define TEST_INPUT 15
#define LED 2

void setup() {
    pinMode(TEST_INPUT, INPUT_PULLUP);
    pinMode(LED, OUTPUT);
}

void loop() {
    digitalWrite(LED, digitalRead(TEST_INPUT));
}