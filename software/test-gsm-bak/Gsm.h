#ifndef Gsm_h
#define Gsm_h

#include "Arduino.h"

typedef enum {
    GET_SYSTEM_VALUES = 0,
    FORCE_OPEN,
    FORCE_CLOSE,
    CALL,
    SIZE_OF_COMMANDS,
    NO_COMMAND = SIZE_OF_COMMANDS
} commands;

typedef enum {
    EMPTY = 0,
    NOT_EMPTY
} messageQueueStates;

class Gsm {
public:
    Gsm(const char (*numbersToSendTo)[13], const int numbersToSendToSize);
    void begin(void) const;
    void sendText(const __FlashStringHelper *pMessage) const;
    void sendText(const char *message) const;
    commands getTextCommand(void);
    void call(void) const;
    void setGsmInSleepMode(void);
    messageQueueStates getMessageQueueState(void) const;

private:
    const char (*_numbersToSendTo)[13];
    const int _numbersToSendToSize;
    char _numberToCall[13];
    bool _isMessageQueueEmpty;
    bool _isGsmInSleepMode;
    void _wakeUpGsmMaybe(void) const;
};

#endif
