#define _SLEEP_DISABLE
#define _STATES_DEBUG

#include "BatteryLevel.h"
#include "Button.h"
#include "Door.h"
#include "Gsm.h"
#include "LightSensor.h"
#include "LowPower.h"
#include "StateMachine.h"

#ifdef _STATES_DEBUG
#define DEBUG_PRINT_STATE_NAME                      \
    {                                               \
        if (currentState != previousCurrentState) { \
            Serial.println(__FUNCTION__);           \
            Serial.flush();                         \
            previousCurrentState = currentState;    \
        }                                           \
    }
#else
#define DEBUG_PRINT_STATE_NAME
#endif

typedef enum { DAY = 0, NIGHT } dayNightPeriods;
#define TIME_SINCE_IDLE (millis() - timeLastIdle)

// states
void stSleep(void);
void stIdle(void);
void stOpenDoor(void);
void stCloseDoor(void);
void stDoorCloseSuccess(void);
void stDoorCloseSetError(void);
void stDoorOpenSetError(void);
void stDoorOpenSetError(void);
void stDoorOpenUnsetError(void);
void stBatteryLevelUnsetError(void);
void stGetCommandText(void);
void stSendSystemValuesText(void);
void stVoiceCall(void);
void stButtonPressed(void);
void stButtonSetError(void);
void stButtonUnsetError(void);

// transitions
bool trNext(void);
bool trIsSleepDelayElapsed(void);
bool trIsNewDay(void);
bool trIsDoorOpened(void);
bool trIsNewNight(void);
bool trIsDoorClosed(void);
bool trIsDoorClosedTimedOut(void);
bool trIsDoorOpenDuringNight(void);
bool trIsDoorCloseDuringDay(void);
bool trIsDoorOpenDuringDay(void);
bool trIsBatteryLevelLow(void);
bool trIsBatteryLevelHighEnough(void);
bool trIsBatteryLevelHighEnoughForGsm(void);
bool trIsCommandGetSystemValues(void);
bool trIsCommandVoiceCall(void);
bool trIsCommandForceOpen(void);
bool trIsCommandForceClose(void);
bool trIsButtonPressed(void);
bool trIsButtonReleased(void);
bool trIsButtonLongPressedAndDoorOpen(void);
bool trIsButtonLongPressedAndDoorClose(void);
bool trIsNothingToDo(void);

// globals
dayNightPeriods dayNightPeriod = DAY;
commands command = NO_COMMAND;
doorStates doorState = OPEN;
unsigned long timeLastIdle = 0;

// pin descriptions
const int LIFE_SIGNAL_PIN = 13;
const int LIGHT_SENSOR_PIN = A0;
const int LIGHT_SENSOR_ENABLE_PIN = 7;
const int BATTERY_LEVEL_PIN = A1;
const int MOTOR_UP_PIN = 2;
const int MOTOR_DOWN_PIN = 3;
const int DOOR_SENSOR_PIN = 5;
const int BUTTON_PIN = 11;
// GSM_RX_PIN = 8;
// GSM_TX_PIN = 9;

// objects definition
const LightSensor lightSensor(LIGHT_SENSOR_PIN, LIGHT_SENSOR_ENABLE_PIN);
BatteryLevel batteryLevel(BATTERY_LEVEL_PIN);
Door door(MOTOR_UP_PIN, MOTOR_DOWN_PIN, DOOR_SENSOR_PIN, batteryLevel);
Button button(BUTTON_PIN);
// const char NumbersToSendTo[][13] = {"+33658438582", "+33665099317"};
const char NumbersToSendTo[][13] = {"+33647604130"};
Gsm gsm(NumbersToSendTo, sizeof(NumbersToSendTo) / sizeof(NumbersToSendTo[0]));

void setup() {
#ifdef _STATES_DEBUG
    // Serial.begin(9600);
#endif
    lightSensor.begin();
    batteryLevel.begin();
    door.begin();
    button.begin();
    gsm.begin();

    pinMode(LIFE_SIGNAL_PIN, OUTPUT);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
}
#ifdef _STATES_DEBUG
stateFunction currentState = stIdle;
stateFunction previousCurrentState = NULL;

#endif
void loop() {
#ifndef _STATES_DEBUG
    static stateFunction currentState = stIdle;
#endif

    const stateMachineRule stateMachineArray[] = {
        // clang-format off
            {stSleep,                           trIsSleepDelayElapsed,             stIdle},

            // light sensor door management
            {stIdle,                            trIsNewDay,                        stOpenDoor},
            {stOpenDoor,                        trIsDoorOpened,                    stIdle},

            {stIdle,                            trIsNewNight,                      stCloseDoor},
            {stCloseDoor,                       trIsDoorClosed,                    stDoorCloseSuccess},
            {stDoorCloseSuccess,                trNext,                            stIdle},
            {stCloseDoor,                       trIsDoorClosedTimedOut,            stIdle},

            {stIdle,                            trIsDoorOpenDuringNight,           stDoorCloseSetError},
            {stDoorCloseSetError,               trNext,                            stIdle},

            {stIdle,                            trIsDoorCloseDuringDay,            stDoorOpenSetError},
            {stDoorOpenSetError,                trNext,                            stIdle},

            {stIdle,                            trIsDoorOpenDuringDay,             stDoorOpenUnsetError},
            {stDoorOpenUnsetError,              trNext,                            stIdle},
            
            // battery level management
            {stIdle,                            trIsBatteryLevelLow,               stBatteryLevelSetError},
            {stBatteryLevelSetError,            trNext,                            stIdle},
            {stIdle,                            trIsBatteryLevelHighEnough,        stBatteryLevelUnsetError},
            {stBatteryLevelUnsetError, trNext,  stIdle},

            // text commands management
            {stIdle,                            trIsBatteryLevelHighEnoughForGsm,  stGetCommandText},
            {stIdle,                            trNext,                            stGetCommandText},
            {stGetCommandText,                  trIsCommandGetSystemValues,        stSendSystemValuesText},
            {stSendSystemValuesText,            trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandVoiceCall,              stVoiceCall},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandForceOpen,              stOpenDoor},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandForceClose,             stCloseDoor},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trNext,                            stIdle},

            // button management
            {stIdle,                            trIsButtonPressed,                 stButtonPressed},
            {stButtonPressed,                   trIsButtonPressedError,            stButtonSetError},
            {stButtonSetError,                  trNext,                            stIdle},
            {stIdle,                            trIsButtonErrorAndReleased,        stButtonUnsetError},
            {stButtonUnsetError,                trNext,                            stIdle},
            {stButtonPressed,                   trIsButtonLongPressedAndDoorOpen,  stCloseDoor},
            {stButtonPressed,                   trIsButtonLongPressedAndDoorClose, stOpenDoor},
            {stButtonPressed,                   trIsButtonReleased,                stIdle},

            {stIdle,                            trIsNothingToDo,                   stSleep},
        // clang-format on
    };

    const StateMachine stateMachine(
        stateMachineArray,
        sizeof(stateMachineArray) / sizeof(stateMachineRule));
    currentState = stateMachine.getNextState(currentState);
    currentState();
}

void stSleep(void) {
    DEBUG_PRINT_STATE_NAME
#ifdef _SLEEP_DISABLE
    delay(2000);
#else
    gsm.setGsmInSleepMode();
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    LowPower.powerDown(SLEEP_250MS, ADC_OFF, BOD_OFF);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
#endif
}

void stIdle(void) {
    DEBUG_PRINT_STATE_NAME
    // Serial.println(__FUNCTION__);
    door.standBy();
    timeLastIdle = millis();
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
}

void stOpenDoor(void) {
    DEBUG_PRINT_STATE_NAME
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    door.up();
    doorState = OPEN;
}

void stCloseDoor(void) {
    DEBUG_PRINT_STATE_NAME
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    door.down();
    doorState = CLOSE;
}

void stDoorCloseSuccess(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("Porte fermée avec succès ! Côt côôt !");
    door.setError(CLOSE, false);
    door.setError(OPEN, false);
}

void stDoorCloseSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("ATTENTION !! Porte non fermée !");
    door.setError(CLOSE, true);
}

void stDoorOpenUnsetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("Porte re-ouverte avec succès.");
    door.setError(OPEN, false);
    door.setError(CLOSE, false);
}

void stDoorOpenSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("ATTENTION !! Porte fermée de manière impromptue !");
    door.setError(OPEN, true);
}

void stBatteryLevelSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("ATTENTION !! Niveau batterie faible !");
    batteryLevel.setError(true);
}
void stBatteryLevelUnsetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText("Niveau batterie suffisant.");
    batteryLevel.setError(false);
}
void stGetCommandText(void) {
    DEBUG_PRINT_STATE_NAME
    command = gsm.getTextCommand();
}

void stSendSystemValuesText(void) {
    DEBUG_PRINT_STATE_NAME
    char message[100];
    int size = sprintf(message,
                       "Niveau batterie: %dmv\n"
                       "Luminosité: %d\n"
                       "Capteur de porte: %s\n"
                       "Periode: %s\n"
                       "Boutton: %s",
                       batteryLevel.getMilliVoltValue(), lightSensor.getValue(),
                       (door.getState() == OPEN) ? "ouverte" : "fermée",
                       (dayNightPeriod == DAY) ? "jour" : "nuit",
                       (button.getState() == RELEASED) ? "relaché" : "pressé");
    gsm.sendText(message);
}

void stVoiceCall(void) {
    // DEBUG_PRINT_STATE_NAME
    gsm.call();
}

void stButtonPressed(void) {
    DEBUG_PRINT_STATE_NAME
}

void stButtonSetError(void) {
    // DEBUG_PRINT_STATE_NAME
    gsm.sendText("ATTENTION !! Bouton poussoir bloqué !");
    button.setError(true);
}

void stButtonUnsetError(void) {
    // DEBUG_PRINT_STATE_NAME
    gsm.sendText("Bouton poussoir débloqué.");
    button.setError(false);
}

bool trNext(void) {
    return true;
}

bool trIsSleepDelayElapsed(void) {
#ifndef _SLEEP_DISABLE
    const int WAKE_UP_LIMIT = 90;
#else
    const int WAKE_UP_LIMIT = 0;
#endif
    static int wakeUpCounter = 0;

    if (wakeUpCounter == WAKE_UP_LIMIT) {
        wakeUpCounter = 0;
        return true;
    }
    wakeUpCounter += 1;
    return false;
}

bool trIsNewDay(void) {
    const int DAY_LIGHT_THRESHOLD = 150;
    if (dayNightPeriod == NIGHT &&
        lightSensor.getValue() > DAY_LIGHT_THRESHOLD) {
        dayNightPeriod = DAY;
        return true;
    }
    return false;
}

bool trIsDoorOpened(void) {
    const int DOOR_OPENING_TIME = 3000;
    return TIME_SINCE_IDLE > DOOR_OPENING_TIME;
}

bool trIsNewNight(void) {
    const int NIGHT_LIGHT_THRESHOLD = 100;
    if (dayNightPeriod == DAY &&
        lightSensor.getValue() < NIGHT_LIGHT_THRESHOLD) {
        dayNightPeriod = NIGHT;
        return true;
    }
    return false;
}

bool trIsDoorClosed(void) {
    return door.getState() == CLOSE;
}

bool trIsDoorClosedTimedOut(void) {
    const int DOOR_CLOSING_TIMEOUT = 10000;
    return TIME_SINCE_IDLE > DOOR_CLOSING_TIMEOUT;
}

bool trIsDoorOpenDuringNight(void) {
    return !door.getError(CLOSE) && door.getState() == OPEN &&
           dayNightPeriod == NIGHT;
}

bool trIsDoorCloseDuringDay(void) {
    return !door.getError(OPEN) && door.getState() == CLOSE &&
           dayNightPeriod == DAY;
}

bool trIsDoorOpenDuringDay(void) {
    return door.getError(OPEN) && door.getState() == OPEN &&
           dayNightPeriod == DAY;
}

bool trIsBatteryLevelLow(void) {
    const int LOW_BATTERY_THRESHOLD = 3200;
    return !batteryLevel.getError() &&
           batteryLevel.getMilliVoltValue() < LOW_BATTERY_THRESHOLD;
}

bool trIsBatteryLevelHighEnough(void) {
    const int LOW_BATTERY_THRESHOLD = 3400;
    return batteryLevel.getError() &&
           batteryLevel.getMilliVoltValue() > LOW_BATTERY_THRESHOLD;
}

bool trIsBatteryLevelHighEnoughForGsm(void) {
    const int BATTERY_HIGH_ENOUGH_FOR_GSM_THRESHOLD = 3800;
    return batteryLevel.getMilliVoltValue() >
           BATTERY_HIGH_ENOUGH_FOR_GSM_THRESHOLD;
}

bool trIsCommandGetSystemValues(void) {
    if (command == GET_SYSTEM_VALUES) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandVoiceCall(void) {
    if (command == CALL) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandForceOpen(void) {
    if (command == FORCE_OPEN) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandForceClose(void) {
    if (command == FORCE_CLOSE) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsButtonPressed(void) {
    return (button.getState() == PRESSED);
}

bool trIsButtonReleased(void) {
    return (button.getState() == RELEASED);
}

bool trIsButtonPressedError(void) {
    const int BUTTON_PRESS_ERROR = 20000;
    return TIME_SINCE_IDLE > BUTTON_PRESS_ERROR;
}

bool trIsButtonErrorAndReleased(void) {
    return button.getState() == RELEASED && button.getError();
}

const int LONG_BUTTON_PRESS = 2000;
bool trIsButtonLongPressedAndDoorOpen(void) {
    if (button.getState() == RELEASED && !button.getError() &&
        TIME_SINCE_IDLE > LONG_BUTTON_PRESS && doorState == OPEN) {
        timeLastIdle = millis();
        return true;
    }
    return false;
}

bool trIsButtonLongPressedAndDoorClose(void) {
    if (button.getState() == RELEASED && !button.getError() &&
        TIME_SINCE_IDLE > LONG_BUTTON_PRESS && doorState == CLOSE) {
        timeLastIdle = millis();
        return true;
    }
    return false;
}

bool trIsNothingToDo(void) {
    Serial.print("command: ");
    Serial.println(command);
    Serial.flush();
    Serial.print("message queue: ");
    Serial.println(gsm.getMessageQueueState());
    Serial.flush();
    return command == NO_COMMAND && gsm.getMessageQueueState() == EMPTY;
}
