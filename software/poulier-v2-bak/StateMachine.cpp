#include "StateMachine.h"

StateMachine::StateMachine(const stateMachineRule *stateMachineArray,
                           const int stateMachineSize)
    : _stateMachineArray(stateMachineArray),
      _stateMachineSize(stateMachineSize) {}

stateFunction StateMachine::getNextState(stateFunction currentState) const {
    static int ruleIndex = 0;
    while (ruleIndex < _stateMachineSize) {
        if ((_stateMachineArray[ruleIndex].sourceState == currentState) &&
            (_stateMachineArray[ruleIndex].transition())) {
            return _stateMachineArray[ruleIndex].destinationState;
        }
        ruleIndex += 1;
    }
    ruleIndex = 0;

    // for (int ruleIndex = 0; ruleIndex < _stateMachineSize; ruleIndex++) {
    //     if ((_stateMachineArray[ruleIndex].sourceState == currentState) &&
    //         (_stateMachineArray[ruleIndex].transition())) {
    //         return _stateMachineArray[ruleIndex].destinationState;
    //     }
    // }
    return currentState;
}
