#define _GSM_DEBUG
#define _GSM_DISABLE

#include "Gsm.h"
#ifndef _GSM_DISABLE
#include "BareBoneSim800.h"
#endif

#ifdef _GSM_DEBUG
#define DEBUG_PRINTF(format, ...)                        \
    {                                                    \
        char buf[256];                                   \
        sprintf(buf, (const char *)format, __VA_ARGS__); \
        Serial.println(buf);                             \
        Serial.flush();                                  \
    }
#define DEBUG_PRINT(message) \
    Serial.println(message); \
    Serial.flush();
#else
#define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINT(message)
#endif

typedef struct {
    const commands command;
    const char *keywords[10];
} commandKeyword;

const int MAX_ATTEMPTS = 20;
#ifndef _GSM_DISABLE
BareBoneSim800 sim800;
#endif
Gsm::Gsm(const char (*numbersToSendTo)[13], const int numbersToSendToSize)
    : _numbersToSendTo(numbersToSendTo),
      _numbersToSendToSize(numbersToSendToSize),
      _isMessageQueueEmpty(false),
      _isGsmInSleepMode(true) {
    strcpy(_numberToCall, _numbersToSendTo[0]);
}

void Gsm::begin() const {
#ifndef _GSM_DISABLE
    sim800.begin();
#endif
#ifdef _GSM_DEBUG
    Serial.begin(9600);
    while (!Serial) {
    }
    Serial.println("cooot");
    Serial.flush();
#endif
}

commands Gsm::getTextCommand(void) {
    commandKeyword commandsKeywords[SIZE_OF_COMMANDS] = {
        {GET_SYSTEM_VALUES,
         {"valeurs", "systeme", "système", "lire", "lecture", "etat", "état"}},
        {FORCE_OPEN, {"ouvrir", "ouverture", "ouvert"}},
        {FORCE_CLOSE, {"fermer", "fermeture", "ferme", "fermé"}},
        {CALL, {"appeler", "appel"}}};

    Gsm::_wakeUpGsmMaybe();

    String message = "";
#ifndef _GSM_DISABLE
    static int currentMessageIndex = 0;
    if (sim800.checkNewSMS() ||
        currentMessageIndex < sim800.currentMessageIndex) {
        currentMessageIndex++;
        message = sim800.readSMS(currentMessageIndex);
    } else if (currentMessageIndex > 0) {
        sim800.dellAllSMS();
        currentMessageIndex = 0;
        _isMessageQueueEmpty = true;
    }
#else
    _isMessageQueueEmpty = true;
#endif
#ifdef _GSM_DEBUG
    if (Serial.available()) {
        message = Serial.readStringUntil('\r');
    }
    while (Serial.available() > 0) {
        Serial.read();
    }
#endif
    if (message != "") {
        DEBUG_PRINT(message)
        // return NO_COMMAND;

        message.toLowerCase();
        for (int commandIndex = 0; commandIndex < SIZE_OF_COMMANDS;
             commandIndex++) {
            for (int keywordIndex = 0;
                 commandsKeywords[commandIndex].keywords[keywordIndex];
                 keywordIndex++) {
                if (message.indexOf(commandsKeywords[commandIndex]
                                        .keywords[keywordIndex]) >= 0) {
                    if (commandsKeywords[commandIndex].command == CALL) {
                        const int NUMBER_DELIMITER_SIZE = 12;
                        const int NUMBER_SIZE = 12;
                        const char numberDelimiter[NUMBER_DELIMITER_SIZE + 1] =
                            "\"rec read\",\"";
                        int numberStartIndex =
                            message.indexOf(numberDelimiter) +
                            NUMBER_DELIMITER_SIZE;
                        Serial.print("dest size");
                        Serial.println(sizeof(_numberToCall));
                        Serial.print("src size");
                        Serial.println(
                            message
                                .substring(numberStartIndex,
                                           numberStartIndex + NUMBER_SIZE)
                                .length());
                        Serial.flush();

                        strcpy(_numberToCall,
                               message
                                   .substring(numberStartIndex,
                                              numberStartIndex + NUMBER_SIZE)
                                   .c_str());
                    }
                    return commandsKeywords[commandIndex].command;
                }
            }
        }
    }
    return NO_COMMAND;
}

void Gsm::sendText(const char *message) const {
    Gsm::_wakeUpGsmMaybe();
    for (int numberIndex = 0; numberIndex < _numbersToSendToSize;
         numberIndex++) {
#ifndef _GSM_DISABLE
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.sendSMS(_numbersToSendTo[numberIndex], message)) {
                break;
            }
            delay(1000);
        }
#endif
        // #ifdef _GSM_DEBUG
        // char serialMessage[130];
        // sprintf(serialMessage, "Text to %s: %s",
        // _numbersToSendTo[numberIndex],
        //         message);
        // Serial.println(serialMessage);
        DEBUG_PRINTF("Text to %s: %s\0", _numbersToSendTo[numberIndex], message)
        // #endif
    }
}

void Gsm::call(void) const {
#ifndef _GSM_DISABLE
    sim800.callNumber(_numberToCall);
#endif
    DEBUG_PRINTF("Calling %s\0", _numberToCall);
}

messageQueueStates Gsm::getMessageQueueState(void) const {
    return _isMessageQueueEmpty ? EMPTY : NOT_EMPTY;
}

void Gsm::setGsmInSleepMode(void) {
#ifndef _GSM_DISABLE
    for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        if (sim800.enterSleepMode()) {
            break;
        }
        delay(1000);
    }
    _isGsmInSleepMode = true;
#endif
}

void Gsm::_wakeUpGsmMaybe(void) const {
#ifndef _GSM_DISABLE
    if (_isGsmInSleepMode) {
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.disableSleep()) {
                break;
            }
            delay(1000);
        }
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.isAttached()) {
                break;
            }
            delay(1000);
        }
    }
#endif
}
