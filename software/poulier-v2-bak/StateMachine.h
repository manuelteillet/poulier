#ifndef StateMachine_h
#define StateMachine_h

#include "Arduino.h"

typedef void (*stateFunction)(void);
typedef bool (*transitionFunction)(void);

typedef struct {
    stateFunction sourceState;
    transitionFunction transition;
    stateFunction destinationState;
} stateMachineRule;

class StateMachine {
public:
    StateMachine(const stateMachineRule *stateMachineArray,
                 const int stateMachineSize);
    stateFunction getNextState(stateFunction currentState) const;

private:
    const stateMachineRule *_stateMachineArray;
    const int _stateMachineSize;
};

#endif
