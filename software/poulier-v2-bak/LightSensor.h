#ifndef LightSensor_h
#define LightSensor_h

#include "Arduino.h"

class LightSensor {
public:
    LightSensor(const int lightSensorPin, const int lightSensorEnablePin);
    void begin(void) const;
    int getValue() const;

private:
    const int _lightSensorPin;
    const int _lightSensorEnablePin;
};

#endif
