#ifndef Button_h
#define Button_h

#include <Arduino.h>

typedef enum { RELEASED = 0, PRESSED } buttonStates;

class Button {
public:
    Button(const int buttonPin);
    void begin(void) const;
    buttonStates getState(void) const;
    bool getError(void) const;
    void setError(bool error);

private:
    const int _buttonPin;
    bool _error;
};

#endif
