// **** INCLUDES *****
#include "LowPower.h"

// ancienne config
// int motorUpPin = 10;
// int motorDownPin = 11;
// int ledPin = 13;
// int luminosityPin = A0;
// int enableMotor = 9;
// int enablePhotoResistor = 8;

int motorUpPin = 9;
int motorDownPin = 10;
int ledPin = 13;
int luminosityPin = A0;
int enableMotor = 8;
int enablePhotoResistor = 7;
int doorClosedSensor = 1;
int doorOpenSensor = 2;

// int motorCurrentPin = A5;

int ledState = HIGH;

int wasNightAddress = 0;
int wasDayAddress = 1;

int luminosity = 0;
int motorCurrent = 0;

int nightTrigger = 100;
int dayTrigger = 150;

bool isDay = true;
bool isNight = false;
bool wasNight = true;
bool wasDay = false;

int doorMoveTimeMax = 30000;

int wakeUpCounter = 91;
int wakeUpLimit = 90;

int motorDownPower = 175;
int motorUpPower = 175;

int sensorDebounceTime = 300;

void setup() {
    //    Serial.begin(9600);
    pinMode(doorClosedSensor, INPUT_PULLUP);
    pinMode(doorOpenSensor, INPUT_PULLUP);

    pinMode(enablePhotoResistor, OUTPUT);
    digitalWrite(enablePhotoResistor, LOW);
    //
    pinMode(enableMotor, OUTPUT);
    pinMode(motorUpPin, OUTPUT);
    pinMode(motorDownPin, OUTPUT);
    digitalWrite(enableMotor, LOW);
    digitalWrite(motorUpPin, LOW);
    digitalWrite(motorDownPin, LOW);

    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, HIGH);
    delay(500);
    digitalWrite(ledPin, LOW);
    delay(500);
    digitalWrite(ledPin, HIGH);
    delay(250);
    digitalWrite(ledPin, LOW);
    delay(250);
}

void loop() {
    if (wakeUpCounter > wakeUpLimit) {
        wakeUpCounter = 0;
        //    Serial.print("was night=");
        //    Serial.println(wasNight);
        //    Serial.print("was day=");
        //    Serial.println(wasDay);

        readLuminosity();

        if (wasDay && isNight) {
            closeDoor();
        }

        if (wasNight && isDay) {
            openDoor();
        }

        wasNight = isNight;
        wasDay = isDay;
    }

    digitalWrite(ledPin, HIGH);
    LowPower.powerDown(SLEEP_250MS, ADC_OFF, BOD_OFF);
    digitalWrite(ledPin, LOW);
    wakeUpCounter += 1;
    // Enter power down state for 8 s with ADC and BOD module disabled
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
}

void readLuminosity() {
    digitalWrite(enablePhotoResistor, HIGH);
    delay(500);
    luminosity = analogRead(luminosityPin);
    digitalWrite(enablePhotoResistor, LOW);
    //  Serial.print("lumiere=");
    //  Serial.println(luminosity);
    if (wasNight) {
        if (luminosity > dayTrigger) {
            isDay = true;
            isNight = false;
        }
    }

    if (wasDay) {
        if (luminosity < nightTrigger) {
            isDay = false;
            isNight = true;
        }
    }

    // Serial.print("is day=");
    // Serial.println(isDay);
    // Serial.print("is night=");
    // Serial.println(isNight);
}

void closeDoor() {
    digitalWrite(enableMotor, HIGH);
    delay(500);
    digitalWrite(motorUpPin, LOW);
    //    Serial.println("down!");
    analogWrite(motorDownPin, motorDownPower);
    unsigned long startedWaiting = millis();
    while (digitalRead(doorClosedSensor) == 0 &&
           millis() - startedWaiting <= doorMoveTimeMax) {
    }
    digitalWrite(motorDownPin, LOW);
    digitalWrite(enableMotor, LOW);
}

void openDoor() {
    digitalWrite(enableMotor, HIGH);
    delay(500);
    digitalWrite(motorDownPin, LOW);
    //    Serial.println("up!");
    analogWrite(motorUpPin, motorUpPower);
    unsigned long startedWaiting = millis();

    int doorReallyOpened = false;
    while (!doorReallyOpened && millis() - startedWaiting <= doorMoveTimeMax) {
        unsigned long startedDebounce = millis();
        doorReallyOpened = true;
        while (millis() - startedDebounce <= sensorDebounceTime) {
            if (digitalRead(doorOpenSensor) == 1) {
                doorReallyOpened = false;
            }
        }
    }
    digitalWrite(motorUpPin, LOW);
    digitalWrite(enableMotor, LOW);
}
