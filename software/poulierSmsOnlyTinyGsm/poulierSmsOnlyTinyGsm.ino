#define TINY_GSM_MODEM_SIM800

#include <SoftwareSerial.h>
#include <TinyGsmClient.h>
#include "LowPower.h"

#define MANU_AND_AUDREY_PHONE_NUMBERS \
    { "+33658438598", "+33665099317" }
#define MANU_ONLY_PHONE_NUMBER \
    { "+33658438598" }
#define PHONE_NUMBERS_LIST MANU_AND_AUDREY_PHONE_NUMBERS
// #define _DEBUG

#ifdef _DEBUG
#define DEBUG_BEGIN       \
    Serial.begin(115200); \
    while (!Serial) {     \
    }                     \
    Serial.println("cooot");

#define DEBUG_PRINTF(format, ...)                        \
    {                                                    \
        char buf[150];                                   \
        sprintf(buf, (const char *)format, __VA_ARGS__); \
        Serial.println(buf);                             \
        delay(500);                                      \
    }
#define DEBUG_PRINT(message)     \
    {                            \
        Serial.println(message); \
        delay(500);              \
    }
#else
#define DEBUG_BEGIN
#define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINT(message)
#endif

#define LIFE_SIGNAL_PIN 13
#define DOOR_CLOSED_SENSOR_PIN 3
#define DOOR_OPEN_SENSOR_PIN 2
#define GSM_RX_PIN 8
#define GSM_TX_PIN 9

// Range to attempt to autobaud
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 57600

#define MAX_ATTEMPTS 5

SoftwareSerial serialAT(GSM_RX_PIN, GSM_TX_PIN);
// #define serialAT Serial2
TinyGsm modem(serialAT);

bool isGsmInSleepMode = false;
bool isSensorChangeStronglySuspected = true;
int doorClosedSensorState = 0;
int doorOpenSensorState = 0;
int previousDoorClosedSensorState = 1;
int previousDoorOpenSensorState = 0;
const char NumbersToSendTo[][13] = PHONE_NUMBERS_LIST;
int numbersToSendToSize = sizeof(NumbersToSendTo) / sizeof(NumbersToSendTo[0]);

void hardWakeUp() {
    isSensorChangeStronglySuspected = true;
}

void setup() {
    DEBUG_BEGIN

    serialAT.begin(115200);

    pinMode(DOOR_CLOSED_SENSOR_PIN, INPUT_PULLUP);
    pinMode(DOOR_OPEN_SENSOR_PIN, INPUT_PULLUP);
    pinMode(LIFE_SIGNAL_PIN, OUTPUT);
    for (int i = 0; i < 20; i++) {
        digitalWrite(LIFE_SIGNAL_PIN, HIGH);
        delay(500);
        digitalWrite(LIFE_SIGNAL_PIN, LOW);
        delay(500);
    }
}

void loop() {
    setGsmInSleepMode();
    attachInterrupt(digitalPinToInterrupt(DOOR_CLOSED_SENSOR_PIN), hardWakeUp,
                    CHANGE);
    attachInterrupt(digitalPinToInterrupt(DOOR_OPEN_SENSOR_PIN), hardWakeUp,
                    CHANGE);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    detachInterrupt(digitalPinToInterrupt(DOOR_CLOSED_SENSOR_PIN));
    detachInterrupt(digitalPinToInterrupt(DOOR_OPEN_SENSOR_PIN));
    if (isSensorChangeStronglySuspected) {
        delay(30000);
        isSensorChangeStronglySuspected = false;
    }
    doorClosedSensorState = digitalRead(DOOR_CLOSED_SENSOR_PIN);
    doorOpenSensorState = digitalRead(DOOR_OPEN_SENSOR_PIN);
    DEBUG_PRINTF("Door closed sensor state: %d", doorClosedSensorState)
    DEBUG_PRINTF("Door open sensor state: %d", doorOpenSensorState)
    if ((doorClosedSensorState != previousDoorClosedSensorState) ||
        (doorOpenSensorState != previousDoorOpenSensorState)) {
        wakeUpGsmMaybe();
        for (int numberIndex = 0; numberIndex < numbersToSendToSize;
             numberIndex++) {
            DEBUG_PRINTF("Trying to send message to number: %s",
                         NumbersToSendTo[numberIndex]);

            for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
                if ((doorClosedSensorState == 0) &&
                    (doorOpenSensorState == 0)) {
                    if (modem.sendSMS(NumbersToSendTo[numberIndex],
                                      "porte ouverte")) {
                        DEBUG_PRINTF("Message porte ouverte sent at attempt %d",
                                     attempt);
                        break;
                    }
                } else if ((doorClosedSensorState == 1) &&
                           (doorOpenSensorState == 1)) {
                    if (modem.sendSMS(NumbersToSendTo[numberIndex],
                                      "porte fermee")) {
                        DEBUG_PRINTF("Message porte fermee sent at attempt %d",
                                     attempt);
                        break;
                    }
                } else if ((doorClosedSensorState == 0) &&
                           (doorOpenSensorState == 1)) {
                    if (modem.sendSMS(NumbersToSendTo[numberIndex],
                                      "porte un peu ouverte")) {
                        DEBUG_PRINTF(
                            "Message porte un peu ouverte sent at attempt %d",
                            attempt);
                        break;
                    }
                } else if ((doorClosedSensorState == 1) &&
                           (doorOpenSensorState == 0)) {
                    if (modem.sendSMS(NumbersToSendTo[numberIndex],
                                      "erreur capteur")) {
                        DEBUG_PRINTF(
                            "Message erreur capteur sent at attempt %d",
                            attempt);
                        break;
                    }
                }
                delay(1000);
            }
        }
        previousDoorClosedSensorState = doorClosedSensorState;
        previousDoorOpenSensorState = doorOpenSensorState;
    }
}

void setGsmInSleepMode(void) {
    if (!isGsmInSleepMode) {
        delay(1000);
        DEBUG_PRINT(F("Trying to go to sleep."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (modem.setPhoneFunctionality(0)) {
                delay(50);
                modem.sendAT(GF("+CSCLK=2\r\n"));
                DEBUG_PRINTF("Gsm sleeping. Attempt: %d", attempt);
                isGsmInSleepMode = true;
                break;
            }
            delay(1000);
        }
        delay(1000);
    }
}

void wakeUpGsmMaybe(void) {
    if (isGsmInSleepMode) {
        DEBUG_PRINT(F("Trying to wake up."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            modem.sendAT("FF");
            delay(120);
            if (modem.sleepEnable(0)) {
                if (modem.setPhoneFunctionality(1)) {
                    DEBUG_PRINTF("Gsm awake. Attempt: %d", attempt)
                    break;
                }
            }
            delay(1000);
        }
        delay(1000);
        DEBUG_PRINT(F("Trying to connect to the network."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (modem.waitForNetwork(60000L)) {
                delay(1000);
                if (modem.isNetworkConnected()) {
                    DEBUG_PRINTF("Network connected. Attempt: %d", attempt)
                    isGsmInSleepMode = false;
                    break;
                }
            }
            if (modem.restart()) {
                DEBUG_PRINT(F("Modem restarted."))
                delay(1000);
            }
            delay(1000);
        }
        delay(1000);
    }
}
