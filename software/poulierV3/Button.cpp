#include "Button.h"

Button::Button(const int buttonPin) : _buttonPin(buttonPin), _error(false) {}

void Button::begin() const {
    pinMode(_buttonPin, INPUT_PULLUP);
}

buttonStates Button::getState() const {
    return digitalRead(_buttonPin) ? RELEASED : PRESSED;
}

bool Button::getError() const {
    return _error;
}

void Button::setError(bool error) {
    _error = error;
}
