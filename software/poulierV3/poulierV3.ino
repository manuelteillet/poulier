#include "BatteryLevel.h"
#include "Button.h"
#include "DebugTools.h"
#include "Door.h"
#include "Gsm.h"
#include "LightSensor.h"
#include "LowPower.h"
#include "StateMachine.h"
#include "config/Poulier.h"

typedef enum { DAY = 0, NIGHT } dayNightPeriods;
#define TIME_SINCE_IDLE (millis() - timeLastIdle)

// states
void stSleep(void);
void stIdle(void);
void stOpenDoor(void);
void stCloseDoor(void);
void trDoorOpenedSuccess(void);
void stDoorCloseSuccess(void);
void stDoorCloseSetError(void);
void stDoorOpenSetError(void);
void stDoorOpenSetError(void);
void stDoorOpenUnsetError(void);
void stBatteryLevelUnsetError(void);
void stGetCommandText(void);
void stSendSystemValuesText(void);
void stVoiceCall(void);
void stButtonPressed(void);
void stButtonSetError(void);
void stButtonUnsetError(void);

// transitions
bool trNext(void);
bool trIsSleepDelayElapsed(void);
bool trIsNewDay(void);
bool trIsDoorOpened(void);
bool trIsNewNight(void);
bool trIsDoorClosed(void);
bool trIsDoorClosedTimedOut(void);
bool trIsDoorOpenDuringNight(void);
bool trIsDoorCloseDuringDay(void);
bool trIsDoorOpenDuringDay(void);
bool trIsBatteryLevelLow(void);
bool trIsBatteryLevelHighEnough(void);
bool trIsBatteryLevelHighEnoughForGsm(void);
bool trIsCommandGetSystemValues(void);
bool trIsCommandVoiceCall(void);
bool trIsCommandForceOpen(void);
bool trIsCommandForceClose(void);
bool trIsButtonPressed(void);
bool trIsButtonReleased(void);
bool trIsButtonLongPressedAndDoorOpen(void);
bool trIsButtonLongPressedAndDoorClose(void);
bool trIsNothingToDo(void);

// globals
RTC_DATA_ATTR dayNightPeriods dayNightPeriod = DAY;
RTC_DATA_ATTR doorStates doorState = OPEN;
commands command = NO_COMMAND;
RTC_DATA_ATTR unsigned long timeLastIdle = 0;

// objects definition
const LightSensor lightSensor(LIGHT_SENSOR_PIN, LIGHT_SENSOR_ENABLE_PIN);
BatteryLevel batteryLevel(BATTERY_LEVEL_PIN);
Door door(MOTOR_UP_PIN, MOTOR_DOWN_PIN, DOOR_SENSOR_PIN, batteryLevel);
Button button(BUTTON_PIN);
const char NumbersToSendTo[][13] = PHONE_NUMBERS_LIST;

Gsm gsm(NumbersToSendTo, sizeof(NumbersToSendTo) / sizeof(NumbersToSendTo[0]));

#ifdef _STATES_DEBUG
stateFunction currentState = stIdle;
stateFunction previousCurrentState = NULL;
#endif
#ifdef _FREE_MEMORY_DEBUG
RTC_DATA_ATTR int previousFreeMemory = 2048;
#endif

const stateMachineRule stateMachineArray[] = {
    // clang-format off
            // button management
            {stIdle,                            trIsButtonPressed,                 stButtonPressed},
            {stButtonPressed,                   trIsButtonPressedError,            stButtonSetError},
            {stButtonSetError,                  trNext,                            stIdle},
            {stIdle,                            trIsButtonErrorAndReleased,        stButtonUnsetError},
            {stButtonUnsetError,                trNext,                            stIdle},
            {stButtonPressed,                   trIsButtonLongPressedAndDoorOpen,  stCloseDoor},
            {stButtonPressed,                   trIsButtonLongPressedAndDoorClose, stOpenDoor},
            {stButtonPressed,                   trIsButtonReleased,                stIdle},

            // light sensor door management
            {stIdle,                            trIsNewDay,                        stOpenDoor},
            {stOpenDoor,                        trIsDoorOpened,                    trDoorOpenedSuccess},
            {trDoorOpenedSuccess,               trNext,                            stIdle},

            {stIdle,                            trIsNewNight,                      stCloseDoor},
            {stCloseDoor,                       trIsDoorClosed,                    stDoorCloseSuccess},
            {stDoorCloseSuccess,                trNext,                            stIdle},
            {stCloseDoor,                       trIsDoorClosedTimedOut,            stIdle},

            {stIdle,                            trIsDoorOpenDuringNight,           stDoorCloseSetError},
            {stDoorCloseSetError,               trNext,                            stIdle},

            {stIdle,                            trIsDoorCloseDuringDay,            stDoorOpenSetError},
            {stDoorOpenSetError,                trNext,                            stIdle},

            {stIdle,                            trIsDoorOpenDuringDay,             stDoorOpenUnsetError},
            {stDoorOpenUnsetError,              trNext,                            stIdle},
            
            // battery level management
            {stIdle,                            trIsBatteryLevelLow,               stBatteryLevelSetError},
            {stBatteryLevelSetError,            trNext,                            stIdle},
            {stIdle,                            trIsBatteryLevelHighEnough,        stBatteryLevelUnsetError},
            {stBatteryLevelUnsetError, trNext,  stIdle},

            // text commands management
            {stIdle,                            trIsBatteryLevelHighEnoughForGsm,  stGetCommandText},
            {stIdle,                            trNext,                            stGetCommandText},
            {stGetCommandText,                  trIsCommandGetSystemValues,        stSendSystemValuesText},
            {stSendSystemValuesText,            trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandVoiceCall,              stVoiceCall},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandForceOpen,              stOpenDoor},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trIsCommandForceClose,             stCloseDoor},
            {stVoiceCall,                       trNext,                            stIdle},
            {stGetCommandText,                  trNext,                            stIdle},

            {stIdle,                            trIsNothingToDo,                   stSleep},
    // clang-format on
};

void hardWakeUp() {}

void setup() {
#ifdef _SERIAL_NEEDED_FOR_DEBUG
    Serial.begin(115200);
    while (!Serial) {
    }
#endif

    lightSensor.begin();
    batteryLevel.begin();
    door.begin();
    button.begin();
    gsm.begin();

    pinMode(LIFE_SIGNAL_PIN, OUTPUT);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);

    while (true) {
#ifndef _STATES_DEBUG
        stateFunction currentState = stIdle;
#endif

        DEBUG_PRINT_FREE_MEMORY

        // DEBUG_PRINT_FREE_MEMORY

        const StateMachine stateMachine(
            stateMachineArray,
            sizeof(stateMachineArray) / sizeof(stateMachineRule));

        DEBUG_PRINT_FREE_MEMORY

        currentState();

        DEBUG_PRINT_FREE_MEMORY

        currentState = stateMachine.getNextState(currentState);
    }
}

void stSleep(void) {
    DEBUG_PRINT_STATE_NAME
#ifdef _SLEEP_DISABLE
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    delay(2000);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
#else
    gsm.setGsmInSleepMode();
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), hardWakeUp, LOW);
    attachInterrupt(digitalPinToInterrupt(DOOR_SENSOR_PIN), hardWakeUp, CHANGE);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    detachInterrupt(digitalPinToInterrupt(BUTTON_PIN));
    detachInterrupt(digitalPinToInterrupt(DOOR_SENSOR_PIN));
#endif
}

void stIdle(void) {
    DEBUG_PRINT_STATE_NAME
    door.standBy();
    timeLastIdle = millis();
    // digitalWrite(LIFE_SIGNAL_PIN, LOW);
}

void stOpenDoor(void) {
    DEBUG_PRINT_STATE_NAME
    // digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    door.up();
    doorState = OPEN;
}

void stCloseDoor(void) {
    DEBUG_PRINT_STATE_NAME
    // digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    door.down();
    doorState = CLOSE;
}

void trDoorOpenedSuccess(void) {
    DEBUG_PRINT_STATE_NAME
    door.standBy();
    gsm.sendText(F("Porte ouverte ! Cot coot !"));
}

void stDoorCloseSuccess(void) {
    DEBUG_PRINT_STATE_NAME
    door.standBy();
    gsm.sendText(F("Porte fermee avec succes ! Cot coot !"));
    door.setError(CLOSE, false);
    door.setError(OPEN, false);
}

void stDoorCloseSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("ATTENTION !! Porte non fermee !"));
    door.setError(CLOSE, true);
}

void stDoorOpenUnsetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("Porte re-ouverte avec succes."));
    door.setError(OPEN, false);
    door.setError(CLOSE, false);
}

void stDoorOpenSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("ATTENTION !! Porte fermee de maniere impromptue !"));
    door.setError(OPEN, true);
}

void stBatteryLevelSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("ATTENTION !! Niveau batterie faible !"));
    batteryLevel.setError(true);
}
void stBatteryLevelUnsetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("Niveau batterie suffisant."));
    batteryLevel.setError(false);
}
void stGetCommandText(void) {
    DEBUG_PRINT_STATE_NAME
    command = gsm.getTextCommand();
}

void stSendSystemValuesText(void) {
    DEBUG_PRINT_STATE_NAME
    char message[100];
    int size = sprintf(message,
                       "Niveau batterie: %dmv\n"
                       "Luminosite: %d\n"
                       "Capteur de porte: %s\n"
                       "Periode: %s\n"
                       "Bouton: %s",
                       batteryLevel.getMilliVoltValue(), lightSensor.getValue(),
                       (door.getState() == OPEN) ? "ouverte" : "fermee",
                       (dayNightPeriod == DAY) ? "jour" : "nuit",
                       (button.getState() == RELEASED) ? "relache" : "presse");
    gsm.sendText(message);
}

void stVoiceCall(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.call();
}

void stButtonPressed(void) {
    DEBUG_PRINT_STATE_NAME
}

void stButtonSetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("ATTENTION !! Bouton poussoir bloque !"));
    button.setError(true);
}

void stButtonUnsetError(void) {
    DEBUG_PRINT_STATE_NAME
    gsm.sendText(F("Bouton poussoir debloque."));
    button.setError(false);
}

bool trNext(void) {
    return true;
}

bool trIsSleepDelayElapsed(void) {
#ifdef _SLEEP_DISABLE
    return true;
#endif
    if (wakeUpCounter == WAKE_UP_LIMIT) {
        wakeUpCounter = 0;
        return true;
    }
    wakeUpCounter += 1;
    return false;
}

bool trIsNewDay(void) {
    const int DAY_LIGHT_THRESHOLD = 750;
    if (dayNightPeriod == NIGHT &&
        lightSensor.getValue() < DAY_LIGHT_THRESHOLD) {
        dayNightPeriod = DAY;
        return true;
    }
    return false;
}

bool trIsDoorOpened(void) {
    const int DOOR_OPENING_TIME = 5000;
    return TIME_SINCE_IDLE > DOOR_OPENING_TIME;
}

bool trIsNewNight(void) {
    const int NIGHT_LIGHT_THRESHOLD = 800;
    if (dayNightPeriod == DAY &&
        lightSensor.getValue() > NIGHT_LIGHT_THRESHOLD) {
        dayNightPeriod = NIGHT;
        return true;
    }
    return false;
}

bool trIsDoorClosed(void) {
    return door.getState() == CLOSE;
}

bool trIsDoorClosedTimedOut(void) {
    const int DOOR_CLOSING_TIMEOUT = 10000;
    return TIME_SINCE_IDLE > DOOR_CLOSING_TIMEOUT;
}

bool trIsDoorOpenDuringNight(void) {
    return !door.getError(CLOSE) && door.getState() == OPEN &&
           dayNightPeriod == NIGHT;
}

bool trIsDoorCloseDuringDay(void) {
    return !door.getError(OPEN) && door.getState() == CLOSE &&
           dayNightPeriod == DAY;
}

bool trIsDoorOpenDuringDay(void) {
    return door.getError(OPEN) && door.getState() == OPEN &&
           dayNightPeriod == DAY;
}

bool trIsBatteryLevelLow(void) {
    const int LOW_BATTERY_THRESHOLD = 3200;
    return !batteryLevel.getError() &&
           batteryLevel.getMilliVoltValue() < LOW_BATTERY_THRESHOLD;
}

bool trIsBatteryLevelHighEnough(void) {
    const int LOW_BATTERY_THRESHOLD = 3400;
    return batteryLevel.getError() &&
           batteryLevel.getMilliVoltValue() > LOW_BATTERY_THRESHOLD;
}

bool trIsBatteryLevelHighEnoughForGsm(void) {
    const int BATTERY_HIGH_ENOUGH_FOR_GSM_THRESHOLD = 3800;
    return batteryLevel.getMilliVoltValue() >
           BATTERY_HIGH_ENOUGH_FOR_GSM_THRESHOLD;
}

bool trIsCommandGetSystemValues(void) {
    if (command == GET_SYSTEM_VALUES) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandVoiceCall(void) {
    if (command == CALL) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandForceOpen(void) {
    if (command == FORCE_OPEN) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsCommandForceClose(void) {
    if (command == FORCE_CLOSE) {
        command = NO_COMMAND;
        return true;
    }
    return false;
}

bool trIsButtonPressed(void) {
    return (button.getState() == PRESSED);
}

bool trIsButtonReleased(void) {
    return (button.getState() == RELEASED);
}

bool trIsButtonPressedError(void) {
    const int BUTTON_PRESS_ERROR = 20000;
    return TIME_SINCE_IDLE > BUTTON_PRESS_ERROR;
}

bool trIsButtonErrorAndReleased(void) {
    return button.getState() == RELEASED && button.getError();
}

const int LONG_BUTTON_PRESS = 2000;
bool trIsButtonLongPressedAndDoorOpen(void) {
    if (button.getState() == RELEASED && !button.getError() &&
        TIME_SINCE_IDLE > LONG_BUTTON_PRESS && doorState == OPEN) {
        timeLastIdle = millis();
        return true;
    }
    return false;
}

bool trIsButtonLongPressedAndDoorClose(void) {
    if (button.getState() == RELEASED && !button.getError() &&
        TIME_SINCE_IDLE > LONG_BUTTON_PRESS && doorState == CLOSE) {
        timeLastIdle = millis();
        return true;
    }
    return false;
}

bool trIsNothingToDo(void) {
    return command == NO_COMMAND && gsm.getMessageQueueState() == EMPTY;
}
