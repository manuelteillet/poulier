#ifndef ConfigPoulier_h
#define ConfigPoulier_h

// settings
// #define _SLEEP_DISABLE
// #define _GSM_DISABLE

#define _STATES_DEBUG
#define _FREE_MEMORY_DEBUG
#define _GSM_DEBUG

#define MANU_AND_AUDREY_PHONE_NUMBERS \
    { "+33658438598", "+33665099317" }
#define MANU_ONLY_PHONE_NUMBER \
    { "+33658438598" }
#define PHONE_NUMBERS_LIST MANU_ONLY_PHONE_NUMBER

#define SLEEP_TIME_MINUTES 15
#define DEBUG_SLEEP_TIME_S 2

// sleep time
#if defined(_STATES_DEBUG) || defined(_FREE_MEMORY_DEBUG) || defined(_GSM_DEBUG)
#define SLEEP_TIME_S DEBUG_SLEEP_TIME_S
#else
#define SLEEP_TIME_S SLEEP_TIME_MINUTES * 60
#endif

// pin descriptions
#define LIFE_SIGNAL_PIN 2
// #define LIGHT_SENSOR_PIN 36
// #define LIGHT_SENSOR_ENABLE_PIN 34
// #define BATTERY_LEVEL_PIN 39
#define MOTOR_UP_PIN 5
#define MOTOR_DOWN_PIN 18
#define DOOR_SENSOR_UP_PIN 17
#define DOOR_SENSOR_DOWN_PIN 14
#define BUTTON_PIN 4
// GSM_RX_PIN 16
// GSM_TX_PIN 17

#endif