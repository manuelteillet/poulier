#include "Door.h"

Door::Door(const int motorUpPin,
           const int motorDownPin,
           const int doorSensorPin,
           BatteryLevel batteryLevel)
    : _motorUpPin(motorUpPin),
      _motorDownPin(motorDownPin),
      _doorSensorPin(doorSensorPin),
      _batteryLevel(batteryLevel),
      _error{false, false} {}

void Door::begin() const {
    pinMode(_motorUpPin, OUTPUT);
    pinMode(_motorDownPin, OUTPUT);
    pinMode(_doorSensorPin, INPUT_PULLUP);
}

void Door::up() const {
    // BatteryLevel = 3V -> 3V = 255
    // BatteryLevel = 5V -> 3V = 3*255/5 = 153
    // y = 765000 / x(mV)
    analogWrite(_motorUpPin,
                min(700000 / _batteryLevel.getMilliVoltValue(), 245));
    digitalWrite(_motorDownPin, LOW);
}

void Door::down() const {
    digitalWrite(_motorUpPin, LOW);
    analogWrite(_motorDownPin,
                min(700000 / _batteryLevel.getMilliVoltValue(), 245));
}

void Door::standBy() const {
    digitalWrite(_motorUpPin, LOW);
    digitalWrite(_motorDownPin, LOW);
}

doorStates Door::getState() const {
    return digitalRead(_doorSensorPin) ? CLOSE : OPEN;
}

void Door::setError(doorStates doorState, bool error) {
    _error[doorState] = error;
}

bool Door::getError(doorStates doorState) {
    return _error[doorState];
}
