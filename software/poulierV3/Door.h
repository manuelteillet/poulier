#ifndef Door_h
#define Door_h

#include "Arduino.h"
#include "BatteryLevel.h"

typedef enum { OPEN = 0, CLOSE, SIZE_OF_DOOR_STATES } doorStates;

class Door {
public:
    Door(const int motorUpPin,
         const int motorDownPin,
         const int doorSensorPin,
         BatteryLevel batteryLevel);
    void begin(void) const;
    void up() const;
    void down() const;
    void standBy() const;
    doorStates getState() const;
    void setError(doorStates doorState, bool error);
    bool getError(doorStates doorState);

private:
    const int _motorUpPin;
    const int _motorDownPin;
    const int _doorSensorPin;
    BatteryLevel _batteryLevel;
    bool _error[SIZE_OF_DOOR_STATES];
};

#endif
