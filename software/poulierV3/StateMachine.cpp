#include "StateMachine.h"

StateMachine::StateMachine(const stateMachineRule *stateMachineArray,
                           const int stateMachineSize)
    : _stateMachineArray(stateMachineArray),
      _stateMachineSize(stateMachineSize) {}

stateFunction StateMachine::getNextState(stateFunction currentState) const {
    static int ruleIndex = 0;
    static int previousRuleIndex = 0;
    do {
        if ((_stateMachineArray[ruleIndex].sourceState == currentState) &&
            (_stateMachineArray[ruleIndex].transition())) {
            previousRuleIndex = ruleIndex;
            return _stateMachineArray[ruleIndex].destinationState;
        }
        ruleIndex += 1;
        ruleIndex %= _stateMachineSize;
    } while (ruleIndex != previousRuleIndex);

    return currentState;
}
