#include "BatteryLevel.h"

BatteryLevel::BatteryLevel(int batteryLevelPin)
    : _batteryLevelPin(batteryLevelPin), _error(false) {}

void BatteryLevel::begin() const {
    pinMode(_batteryLevelPin, INPUT);
}

unsigned int BatteryLevel::getMilliVoltValue() const {
    // 1023 -> 1.25V
    // 256  -> 5V
    // y(mV) = 1278750 / x
    return 1278750 / analogRead(_batteryLevelPin);
}

bool BatteryLevel::getError() const {
    return _error;
}

void BatteryLevel::setError(bool error) {
    _error = error;
}
