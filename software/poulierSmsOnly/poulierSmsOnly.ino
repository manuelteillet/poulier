#include "BareBoneSim800.h"
#include "LowPower.h"

#define MANU_AND_AUDREY_PHONE_NUMBERS \
    { "+33658438598", "+33665099317" }
#define MANU_ONLY_PHONE_NUMBER \
    { "+33658438598" }
#define PHONE_NUMBERS_LIST MANU_ONLY_PHONE_NUMBER
// #define _DEBUG

#ifdef _DEBUG
#define DEBUG_BEGIN     \
    Serial.begin(9600); \
    while (!Serial) {   \
    }                   \
    Serial.println("cooot");

#define DEBUG_PRINTF(format, ...)                        \
    {                                                    \
        char buf[150];                                   \
        sprintf(buf, (const char *)format, __VA_ARGS__); \
        Serial.println(buf);                             \
        delay(500);                                      \
    }
#define DEBUG_PRINT(message)     \
    {                            \
        Serial.println(message); \
        delay(500);              \
    }
#else
#define DEBUG_BEGIN
#define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINT(message)
#endif

#define LIFE_SIGNAL_PIN 13
#define DOOR_SENSOR_PIN 3
// GSM_RX_PIN = 8;
// GSM_TX_PIN = 9;

#define MAX_ATTEMPTS 5

BareBoneSim800 sim800;
bool isGsmInSleepMode = true;
int doorSensorState = 0;
int previousDoorSensorState = 0;
const char NumbersToSendTo[][13] = PHONE_NUMBERS_LIST;
int numbersToSendToSize = sizeof(NumbersToSendTo) / sizeof(NumbersToSendTo[0]);

void hardWakeUp() {}

void setup() {
    DEBUG_BEGIN

    sim800.begin();
    pinMode(DOOR_SENSOR_PIN, INPUT_PULLUP);
    pinMode(LIFE_SIGNAL_PIN, OUTPUT);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    delay(500);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
}

void loop() {
    setGsmInSleepMode();
    attachInterrupt(digitalPinToInterrupt(DOOR_SENSOR_PIN), hardWakeUp, CHANGE);
    digitalWrite(LIFE_SIGNAL_PIN, LOW);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    digitalWrite(LIFE_SIGNAL_PIN, HIGH);
    detachInterrupt(digitalPinToInterrupt(DOOR_SENSOR_PIN));
    delay(100);
    doorSensorState = digitalRead(DOOR_SENSOR_PIN);
    DEBUG_PRINTF("Door sensor state: %d", doorSensorState)
    if (doorSensorState != previousDoorSensorState) {
        wakeUpGsmMaybe();
        for (int numberIndex = 0; numberIndex < numbersToSendToSize;
             numberIndex++) {
            DEBUG_PRINTF("Trying to send message to number: %s",
                         NumbersToSendTo[numberIndex]);

            for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
                if (doorSensorState == 0) {
                    if (sim800.sendSMS(NumbersToSendTo[numberIndex],
                                       "porte ouverte")) {
                        DEBUG_PRINTF("Message porte ouverte sent at attempt %d",
                                     attempt);
                        break;
                    }
                } else {
                    if (sim800.sendSMS(NumbersToSendTo[numberIndex],
                                       "porte fermee")) {
                        DEBUG_PRINTF("Message porte fermee sent at attempt %d",
                                     attempt);
                        break;
                    }
                }
                delay(1000);
            }
        }
        previousDoorSensorState = doorSensorState;
    }
}

void setGsmInSleepMode(void) {
    if (!isGsmInSleepMode) {
        delay(1000);
        DEBUG_PRINT(F("Trying to go to sleep."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.enterSleepMode()) {
                DEBUG_PRINTF("Gsm sleeping. Attempt: %d", attempt)
                isGsmInSleepMode = true;
                break;
            }
            delay(1000);
        }
        sim800.flushSerial(1000);
        delay(1000);
    }
}

void wakeUpGsmMaybe(void) {
    if (isGsmInSleepMode) {
        DEBUG_PRINT(F("Trying to wake up."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.disableSleep()) {
                DEBUG_PRINTF("Gsm awake. Attempt: %d", attempt)
                break;
            }
            delay(1000);
        }
        delay(1000);
        sim800.flushSerial(1000);
        DEBUG_PRINT(F("Trying to set full mode."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.setFullMode()) {
                DEBUG_PRINTF("Gsm full mode. Attempt: %d", attempt)
                break;
            }
            delay(1000);
        }
        delay(1000);
        sim800.flushSerial(1000);
        DEBUG_PRINT(F("Trying to attach."))
        for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
            if (sim800.isAttached()) {
                DEBUG_PRINTF("Gsm attached. Attempt: %d", attempt)
                isGsmInSleepMode = false;
                break;
            }
            delay(1000);
        }
        // delay(1000);
        // sim800.flushSerial(1000);
        // DEBUG_PRINT(F("Getting SIM number."))
        // String number = "";
        // number = sim800.readSIMNumber();
        // DEBUG_PRINTF("SIM number is: %s", number);
        // delay(1000);
        sim800.flushSerial(1000);
        delay(1000);
    }
}
