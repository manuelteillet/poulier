// #include "LowPower.h"

int motorUpPin = 5;
int motorDownPin = 18;
int motorUpPwm = 0;
int motorDownPwm = 1;
int ledPin = 2;

void setup() {
    //    Serial.begin(9600);

    // pinMode(motorPowerPin, OUTPUT);
    // pinMode(motorUpPin, OUTPUT);
    // pinMode(motorDownPin, OUTPUT);
    pinMode(ledPin, OUTPUT);
    ledcAttachPin(motorUpPin, motorUpPwm);
    ledcAttachPin(motorDownPin, motorDownPwm);
    ledcSetup(motorUpPwm, 30000, 8);
    ledcSetup(motorDownPwm, 30000, 8);

    ledcWrite(motorDownPwm, 0);

    for (int i = 30; i <= 255; i++) {
        ledcWrite(motorUpPwm, i);
        delay(100);
    }

    for (int i = 0; i <= 10; i++) {
        ledcWrite(motorUpPwm, 40 + 20 * i);
        digitalWrite(ledPin, HIGH);
        delay(5000);

        ledcWrite(motorUpPwm, 0);
        digitalWrite(ledPin, LOW);
        delay(1000);
    }
    ledcWrite(motorUpPwm, 250);
    digitalWrite(ledPin, HIGH);
    delay(5000);

    ledcWrite(motorUpPwm, 0);
    digitalWrite(ledPin, LOW);
    delay(1000);

    ledcWrite(motorUpPwm, 255);
    digitalWrite(ledPin, HIGH);
    delay(5000);

    ledcWrite(motorUpPwm, 0);
    digitalWrite(ledPin, LOW);
    delay(1000);
}

void loop() {}
