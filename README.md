# poulier, the whole story

Where I justify myself for my bad design choices.

## etymology

poulier: from "poule" which is chicken in french, with "-ier" endig which means safe.

## first chickens, Jeanine and co.

Our first hen was called Jeanine. She was a nice small silkie chicken and was a gift from a friend. She was the only survivor of a fox attack that killed her 6 roomates the night before her arrival. Jeanine (and our friend) were quite in shock at the time so to give her company we bougth and were offered 6 more chicken (one of them revealing itself to be a rooster. This is important for how the story continues). We also payed attention to go open and close the chicken house door every night and every morning... 

## poulier v1

...until we get bored to have to wakeup on sundays mornings to let our chickens enjoying their day (it took 2 weeks). An arduino uno, a few components, some woodworking and some coding around a few beers with a friend later and the poulier was born. Well at least that is how my mind remebers it. In practice it took me a few weeks (about 8) to get somthing almost reliable to work.

The philosophy of the project was to recycle as many parts as possible.

### mecanical design

Our chicken house was a small room in a small building in the small field we had our small chickens and small sheeps ([ouessan sheeps](https://fr.wikipedia.org/wiki/Ouessant_(race_ovine)) but that's a story for another time) in. I opened a hole in the room door and I built an overhead door out of a recycled galvanised steel plate inspired by designs like [this one](https://www.fermedebeaumont.com/trappe-sortie-aluminium-p-434.html). I dig grooves into two pallet wood planks to work as rails. As my motor was not powerfull enough to carry the door's weight, I used a counterweight (recycled steel rods) and two pulleys (recycled casters with a dremel carved groove). I'm using (recycled) strong fish line (100kg) as wire. 

The steel plate door is exactly around by and mm thick. It weights exacly about ,as the counterweight.

The motor I used is recycled from an old "line following robot" project. Its specs are [here](). The wire is making a loop around the axle of the last reduction gear so when the motor turns in one direction the door goes up and the couterwheight down, and vice-versa. I added a heat srink sleeve around the axle to improve the wire grip.

### hardware

As the chicken house was not on mains I bought a cheap solar powerbank from amazon and I designed a board with power savings in mind, inspired by [this project](https://openhomeautomation.net/arduino-battery)


# current design

## mecanical design

I opened a hole in the chicken house main door and I built an overhead door out of a recycled galvanised steel plate inspired by designs like [this one](https://www.fermedebeaumont.com/trappe-sortie-aluminium-p-434.html). As my motor is not powerfull enough to carry the door's weight, I use a counterweight (recycled steel rods) and two pulleys (recycled casters with a dremel carved groove). I'm using (recycled) strong fish line (100kg) as wire. 

The door mechanism looks like that.
[]

The steel plate door is exactly around by and mm thick. It weights exacly about ,as the counterweight.
[]

I'm using two aluminium profiles of #TODO mm as rails.
[]

The "door opened" sensor is a #TODO reel switch for windows alarm bought online, and the "door closed" sensor is a contact switch recycled from an old walkman.
[]

The motor and the two sensors are wired to the "electronic tupperware" witch holds the battery and the electronic stuff.
[]

On the outside of the door the solar panel, the luminosity sensor and the gsm antenna are wired directly inside the "electronic tuperware" via a hole in the door. The wooden frame is a protection against goats attacks.
[] 

## hardware
I'm using two atmel #TODO chips from arduino uno boards. I mounted them on two dedicated boards directly powered from the battery to avoid power loss from uno bords voltage regulators. One board controls the door and the other one the gsm chip.
I use a cheap solar powerbank as power source (#TODO). The solar panel has been replaced by a new bigger one after beeng damaged by the goats (#TODO).

### the door board

To save as much power as possible, the photoresistor is activated via an outpout pin and the motor driver via a relay. The board design is inspired by [this project](https://makecademy.com/arduino-battery).

### the gsm board

## software

# future improvements

# bad ideas

# alternative possibly good ideas


Pour pimper l'horloge sur quartz externe, utilisation de https://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.json comme gestionnaire de carte supplémentaire.

## configuration vscode

### arduino.json

```
{
    "board": "arduino:avr:uno",
    "sketch": "poulier-v2/poulierV2.ino",
    "port": "/dev/ttyACM0"
}
```

### c_cpp_properties.json

```
{
    "configurations": [
        {
            "name": "arduino",
            "includePath": [
                "${workspaceFolder}/**",
                "/home/manu/Arduino/libraries/**",
                "/usr/include/**",
                "/home/manu/.arduino15/packages/arduino/**"
            ],
            "defines": [
                "__AVR__"
            ],
            "compilerPath": "/usr/bin/clang",
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "clang-x64"
        }
    ],
    "version": 4
}
```

### settings.json

```
  "astyle.c.enable": false,
  "astyle.cpp.enable": false,
  "C_Cpp.clang_format_path": "/usr/bin/clang-format"
```
