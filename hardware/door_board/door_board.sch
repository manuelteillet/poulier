EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328P-PU U?
U 1 1 609D9312
P 3050 3750
F 0 "U?" H 2406 3796 50  0000 R CNN
F 1 "ATmega328P-PU" H 2406 3705 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 3050 3750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 3050 3750 50  0001 C CNN
	1    3050 3750
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:L293D U?
U 1 1 609DED8E
P 6700 2800
F 0 "U?" H 6700 3981 50  0000 C CNN
F 1 "L293D" H 6700 3890 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 6950 2050 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/l293.pdf" H 6400 3500 50  0001 C CNN
	1    6700 2800
	1    0    0    -1  
$EndComp
$Comp
L door_board_library:RY-5W-K K?
U 1 1 609E6AEB
P 4600 2150
F 0 "K?" H 5000 2464 50  0000 C CNN
F 1 "RY-5W-K" H 5000 2373 50  0000 C CNN
F 2 "Fujitsu-RY-0-0-*" H 4600 2650 50  0001 L CNN
F 3 "http://www.fujitsu.com/downloads/MICRO/fcai/relays/ry.pdf" H 4600 2750 50  0001 L CNN
F 4 "Relay" H 4600 2850 50  0001 L CNN "category"
F 5 "150mW" H 4600 2950 50  0001 L CNN "coil power"
F 6 "165Ω" H 4600 3050 50  0001 L CNN "coil resistance"
F 7 "Non-Latching" H 4600 3150 50  0001 L CNN "coil type"
F 8 "1.25A" H 4600 3250 50  0001 L CNN "current rating"
F 9 "Electromechanical" H 4600 3350 50  0001 L CNN "device class L1"
F 10 "Relays" H 4600 3450 50  0001 L CNN "device class L2"
F 11 "Signal Relays" H 4600 3550 50  0001 L CNN "device class L3"
F 12 "12.5mm" H 4600 3650 50  0001 L CNN "height"
F 13 "yes" H 4600 3750 50  0001 L CNN "lead free"
F 14 "5863a36958a10165" H 4600 3850 50  0001 L CNN "library id"
F 15 "Fujitsu" H 4600 3950 50  0001 L CNN "manufacturer"
F 16 "817-RY-5W-K" H 4600 4050 50  0001 L CNN "mouser part number"
F 17 "2" H 4600 4150 50  0001 L CNN "number of contacts"
F 18 "PTH_RELAY_20MM2_9MM8" H 4600 4250 50  0001 L CNN "package"
F 19 "yes" H 4600 4350 50  0001 L CNN "rohs"
F 20 "1A" H 4600 4450 50  0001 L CNN "switching current"
F 21 "24W" H 4600 4550 50  0001 L CNN "switching power"
F 22 "+90°C" H 4600 4650 50  0001 L CNN "temperature range high"
F 23 "-30°C" H 4600 4750 50  0001 L CNN "temperature range low"
F 24 "DPDT" H 4600 4850 50  0001 L CNN "throw configuration"
F 25 "120V" H 4600 4950 50  0001 L CNN "voltage rating AC"
F 26 "24V" H 4600 5050 50  0001 L CNN "voltage rating DC"
	1    4600 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
